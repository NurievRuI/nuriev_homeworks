import java.util.Arrays;

public class Main {

        public static void main(String[] args) {
            int[] myArray = new int[]{132, 0, 54, 0, 0, 8, 12, 79, 100, 0};
            System.out.println("В массиве " + Arrays.toString(myArray) + " номер 12 имеет индекс " + findIndex(myArray, 12));
            System.out.print("Array " + Arrays.toString(myArray) + " после сдвига всех ненулевых элементов влево ");
            zeroLeft(myArray);
            System.out.println("выглядит как " + Arrays.toString(myArray));
        }

        public static int findIndex(int[] myArray, int searchedElement) {
            for(int i = 0; i < myArray.length; ++i) {
                if (myArray[i] == searchedElement) {
                    return i;
                }
            }

            return -1;
        }

        public static void zeroLeft(int[] myArray) {
            int zeroPosition = 0;

            for(int i = 0; i < myArray.length; ++i) {
                if (myArray[i] != 0) {
                    myArray[zeroPosition++] = myArray[i];
                }
            }

            while(zeroPosition < myArray.length) {
                myArray[zeroPosition] = 0;
                ++zeroPosition;
            }

        }
    }
